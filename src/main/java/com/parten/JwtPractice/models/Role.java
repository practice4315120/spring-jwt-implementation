package com.parten.JwtPractice.models;

public enum Role {
    USER,
    ADMIN
}
